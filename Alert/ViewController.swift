//
//  ViewController.swift
//  Alert
//
//  Created by roman on 7/22/21.
//  Copyright © 2021 roman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var hiLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var guessTheNumberLabel: UILabel!
    
        override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func putNameButtonTapped(_ sender: UIButton) {
        self.alertNameAdding(title: "Your name", message: "Put your own name here", style: .alert)
    }
    
    @IBAction func sumButtonTapped(_ sender: UIButton) {
        self.alertSum(title: "Your sum", message: "Put two numbers", style: .alert)
    }
    
    @IBAction func guessThenumberButtonTapped(_ sender: UIButton) {
        self.allertGuessTheNumber(title: "Guess the number", message: "a size of 1MB in KB", style: .alert)
    }
    
    // MARK - you should guess the number(simple game)
    func allertGuessTheNumber(title: String, message: String, style: UIAlertController.Style) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let guess = UIAlertAction(title: "guess", style: .default) { (action) in
            let text = Int(alertController.textFields?[0].text ?? "")!
            if(text == 1024) {
                self.guessTheNumberLabel.text! = "Right!"
            } else {
                self.guessTheNumberLabel.text! = "Wrong!"
            }
        }
        alertController.addTextField { (text) in
            text.placeholder = "Put number"
        }
        alertController.addAction(guess)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK - show sum of two elements in sumLable
    func alertSum(title: String, message: String, style: UIAlertController.Style) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let sum = UIAlertAction(title: "sum", style: .default) { (action) in
            let textFirstLabel = alertController.textFields?[0]
            let textSecondLabel = alertController.textFields?[1]
            let sumOfElements = Int(textFirstLabel?.text ?? "")! + Int(textSecondLabel?.text ?? "")!
            
            self.sumLabel.text! = String(sumOfElements)
        }
        alertController.addTextField { (firstTextField) in
            firstTextField.placeholder = "First number"
        }
        alertController.addTextField { (secondTextField) in
            secondTextField.placeholder = "Second number"
        }
        alertController.addAction(sum)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK - add name to hiLabel
    func alertNameAdding(title: String, message: String, style: UIAlertController.Style) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let addName = UIAlertAction(title: "add", style: .default) { (action) in
            let text = alertController.textFields?.first
            self.hiLabel.text! += (", ") + (text?.text)!
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "Your name"
        }
        alertController.addAction(addName)
        self.present(alertController, animated: true, completion: nil)
    }
}

